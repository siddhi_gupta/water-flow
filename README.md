General Instructions
================
--------------------------------

1. Clone this repo directly. 
2. Edit the script using Sublime or any other text editor.
3. Have the input file in the required format.
4. Run the script using the following command `python waterFlow.py –i inputFile`

Use of Readme
============
------------------------------

* Use Table of Contents to navigate through the sections
* Review the edit log for recent changes
	* Remember to add you signature and date after you review recent changes!
* When you make a change, create an entry at the top of the Edit Log with the date of the change and your signature.

Links
====
----------

* [Breadth First Search](https://en.wikipedia.org/wiki/Breadth-first_search)
* [Depth First Search](https://en.wikipedia.org/wiki/Depth-first_search)
* [Uniform Cost Search](https://en.wikipedia.org/wiki/Uniform-cost_search)


Table of Contents
==============
----------------------------------
[TOC]

Water Flow
================
----------------------------------------

## Problem Statement

>There is an old city that has an antique plumbing system. There are a lot of pipes in the city that are useless or rarely work. We want to find a route for water to traverse from one side of the city to the other side. There is a source of water in which water will start flowing through the system. And there are some destinations which whenever water reaches one of them; we consider the task of routing the water through the city to be done. So, formally speaking, there is one start node (source) and one or more end nodes (destinations) in the city.

>One strange fact about this city is that some of the pipes which are old do not function in specific times. So, the route for water might be different in different times. We are given the time periods when each pipe doesn’t work. But if water is already flowing in a pipe, it will continue flowing even if time reaches one of those periods. We have to make sure that when we want to select the next pipe for the rest of our water route, it is in its working time! Keeping in mind that water cannot stay in one point waiting for a pipe to start working. If we reach a point where there is no pipe open from there, simply this route isn’t a valid route and you have to disregard it.

>We have to keep track of the time too. We assume that it takes n units of time for water to flow through a pipe if the length of the pipe is n. For example if we have two pipes with length 3 and 4 in our route till now and we have started from time 2, current time will be 2 + 3 + 4 = 9.

>Our task is to find a route based on the availability of the pipes in a given time using search algorithms. We are given the start time that water will start flowing and then we have to report the time which water reaches the other side of the city.

>We will be using uninformed search algorithms, i.e. we will be implementing DFS – BFS – UCS for this assignment.

## Input
>We will be given a text input file. First line of this file represents the number of test cases. The next line will be the beginning of the 1st test case. Each test case ends with an empty line. 

> **Each test case consists of the following lines:**

> - <task> algorithm that we are supposed to use for this case
> - <source> name of the source node
> - <destinations> names of the destination nodes
> - <middle nodes> names of the middle nodes
> - <#pipes> represents the number of pipes
> - <graph> represents start-end nodes, lengths and off-times of pipes
> - <start-time> the time when water will start flowing

## Task
This field indicates which algorithm we are going to use to solve the problem. The input is either “BFS”, “DFS” or “UCS” (without the double quotes)

## Source
This is the name of the source of water.

## Destinations
This is a space separated line consisting of names of the destination (goal) nodes.

## Middle Nodes
This is a space separated line consisting of the middle nodes, i.e. nodes that are neither source nor destination. (It may be an empty line which means there are no middle nodes.)

## Pipes
This number represents the number of pipes in the system.

## Graph
This section contains #pipes number of lines. Each line in this section represents one pipe of the system. 
> **Format of each line is as following:**
(There is one space between each field)

<start> <end> <length> <#off-periods> < period1 > .... <periodn> 

>Example: S E 10 3 10-12 15-16 25-29

>It means that this pipe starts from point S , ends in point E , has the length 10, and it has 3 off- periods. It is not working from time 10 to 12, 15 to 16 and 25 to 29. Period 10-12 means that if we are at time 10, 11 or 12 we cannot use this pipe as the next pipe. Some pipes may always work. In that case, the 4th field for these pipes will be 0. Please note that the pipes are unidirectional, i.e. for a pipe that has starting point A and ending point B, the water can flow from A to B only and not in the reverse direction.

>The pipe length will be 1 for both BFS and DFS (i.e. pipe length is ignored by these algorithms and is always assumed to be 1). Also, ignore the off-periods for these algorithms, i.e. when using BFS-DFS; assume that all pipes work all the time.

## Start Time
This is an integer denoting the time when water will start flowing from the source point.

## Output
For each test-case, one per line, report the name of the destination node (case-sensitive, uppercase only) where water reached first and also the time that it reached that node. These two fields should be separated by a space. If no path was found to any of the destination node, i.e. none of the destination nodes can be reached, print “None”.


Edit Log
=======
-----------------
## Complete Creation
> Date: 5/9/15
> 
> **Sections:**
> 
> * General Instructions
> * Use of Readme
> * Links
> * Problem Statement
> * Functions 
>
>
> **Read Signature**
> 
> * Siddhi Gupta: 5/9/15